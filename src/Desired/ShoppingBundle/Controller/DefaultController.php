<?php

namespace Desired\ShoppingBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="main_page")
     * @Template()
     */
    public function indexAction()
    {
        
        $em = $this->getDoctrine()->getManager();

        $productEntities = $em->getRepository('DesiredShoppingBundle:Product')->findAll();
        $categoryEntities = $em->getRepository('DesiredShoppingBundle:Category')->findAll();

        return array(
            'productEntities' => $productEntities,
            'categoryEntities' => $categoryEntities 
        );
    }
}
