<?php

namespace Desired\ShoppingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Sale
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Sale {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreated", type="datetime")
     */
    private $dateCreated;

    /**
     * @ORM\ManyToMany(targetEntity="Product", mappedBy="sales", cascade={"persist"})
     * */
    private $products;
    
     /**
     * @ORM\ManyToOne(targetEntity="Desired\UserBundle\Entity\User", inversedBy="sales")
     * @ORM\JoinColumn(name="userId", referencedColumnName="id")
     **/
    private $user;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }
   
    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     * @return Post
     */
    public function setDateCreated($dateCreated) {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime 
     */
    public function getDateCreated() {
        return $this->dateCreated;
    }
  
    
    /**
     * Set products
     *
     */
     public function setProducts(\Desired\ShoppingBundle\Entity\Product $product) {
        $product->setSales($this); // synchronously updating inverse side          
        $this->products[] = $product;
    }
    
    public function addProduct(\Desired\ShoppingBundle\Entity\Product $product)
    {
        $product->addSales($this);
        $this->products[] = $product;
    }
    
    public function __construct() {
        $this->products = new ArrayCollection();
    }

    /**
     * Get products
     *
     */
    public function getproducts() {
        return $this->products;
    }

    /**
     * Set user
     *
     */
    public function setuser($user) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     */
    public function getuser() {
        return $this->user;
    }
    
    
    
}
