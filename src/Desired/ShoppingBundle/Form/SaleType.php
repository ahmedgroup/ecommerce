<?php

namespace Desired\ShoppingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SaleType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('userId')
            //->add('dateCreated')
            //->add('products')
            //->add('user')
        ;
        
        $builder->add('products', 'entity', array(
            'class' => 'DesiredShoppingBundle:Product',
            'property' => 'name',
            'multiple' => true
        ));
        
       
        //$builder->add('categories',new CategoryType());
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Desired\ShoppingBundle\Entity\Sale'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'desired_shoppingbundle_sale';
    }
}
